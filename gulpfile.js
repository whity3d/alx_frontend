const gulp = require("gulp");
const stylus = require("gulp-stylus");
const pug = require("gulp-pug");
const browserSync = require("browser-sync");
const autoprefixer = require("gulp-autoprefixer");

gulp.task('css',() => {
    return gulp.src('src/stylus/**/*.styl')
    .pipe(stylus())
    .pipe(
        autoprefixer(['last 15 versions', '> 1%', 'ie8', 'ie7'],{
            cascade: true
        })
    )
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({ stream: true }))
})

gulp.task('html',() => {
    return gulp.src('src/pug/**/*.pug')
      .pipe(pug())
      .pipe(gulp.dest('public'))
      .pipe(browserSync.reload({ stream: true }))
  });

gulp.task('browser-sync',() => {
    browserSync({
        server: {
            baseDir: 'public'
        },
        notify: false
    })
})  

gulp.task('default', ['browser-sync','css','html'], () => {
    gulp.watch('src/stylus/**/*.styl', ['css',],
    gulp.watch('src/pug/**/*.pug', ['html']))
});